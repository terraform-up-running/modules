#!/usr/bin/env bash
# user_data.sh - Simple boostrap script to start an one-liner Apache web server.

# Author: Ruben Ricaurte <ricaurtef@gmail.com>

# shellcheck disable=SC2154,SC2086


set -o errexit
set -o nounset


main() {
	cat <<- EOT > index.html
	<style>
	td, th {
		border: 1px solid #dddddd;
		text-align: left;
		padding: 8px;
	}
	</style>
	<table>
		<tr>
			<th colspan="2" style="text-align: center">Hello, world!</th>
		</tr>
		<tr>
			<th>DB address</th>
			<td>${db_address}</td>
		</tr>
		<tr>
			<th>DB port</th>
			<td>${db_port}</td>
		</tr>
	</table>
	EOT

    # Fire up Apache web server.
    nohup busybox httpd -f -p ${server_port} &
}


main "$@"
