output "alb_dns_name" {
  description = "The domain name of the load balancer."
  value       = "http://${aws_lb.this.dns_name}"
}

output "alb_sg_id" {
  description = "The ID of the SG attached to the LB."
  value       = "aws_security_group.this_alb.id"
}
