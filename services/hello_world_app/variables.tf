variable "aws_region" {
  description = "The AWS region to deploy to."
  type        = string
  default     = "us-east-1"
}

variable "db_remote_state_bucket" {
  description = "The name of the S3 bucket for the database's remote state."
  type        = string
}

variable "db_remote_state_key" {
  description = "The path for the database's remote state in S3."
  type        = string
}

variable "environment" {
  description = "The name of the environment to deploy to."
  type        = string

  validation {
    condition     = can(regex("^(stage|prod)$", var.environment))
    error_message = "'environment' Only options allowed: stage or prod."
  }
}

variable "server_port" {
  description = "The port the server will use for HTTP requests."
  type        = number
  default     = 8080
}

variable "server_text" {
  description = "The message the application will display."
  type        = string
  default     = "Hello, world!"
}

variable "ami" {
  description = "The ID of the AMI that should be used to create the instances."
  type        = string
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)."
  type        = string
  default     = "t2.micro"
}

variable "desired_capacity" {
  description = "The desired number of EC2 Instances running at a given time."
  type        = number
}

variable "min_size" {
  description = "The minimum number of EC2 Instances in the ASG."
  type        = number
}

variable "max_size" {
  description = "The maximum number of EC2 Instances in the ASG."
  type        = number
}

variable "scaling_schedule_enabled" {
  description = "Enable scheduled actions in the ASG."
  type        = bool
  default     = false
}

variable "custom_tags" {
  description = "Custom tags to set on the Instances in the ASG."
  type        = map(string)
  default     = {}
}

variable "subnet_ids" {
  description = "The subnet IDs to deploy to."
  type        = list(string)
}
