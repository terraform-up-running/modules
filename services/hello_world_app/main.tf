locals {
  resource_name = "hello-world-${var.environment}"
}

resource "aws_lb_listener_rule" "this" {
  listener_arn = module.alb.alb_http_listener_arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }
}

resource "aws_lb_target_group" "this" {
  name     = local.resource_name
  port     = var.server_port
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}


# Modules setup starts here.
module "asg" {
  source = "../../cluster/asg_rolling_deploy"

  cluster_name  = "hello-world-${var.environment}"
  ami           = var.ami
  instance_type = var.instance_type
  user_data = base64encode(templatefile("${path.module}/user_data.sh", {
    server_port = var.server_port,
    server_text = var.server_text,
    db_address  = data.terraform_remote_state.db.outputs.address,
    db_port     = data.terraform_remote_state.db.outputs.port,
  }))
  desired_capacity         = var.desired_capacity
  min_size                 = var.min_size
  max_size                 = var.max_size
  scaling_schedule_enabled = var.scaling_schedule_enabled
  subnet_ids               = data.aws_subnets.default.ids
  target_group_arns        = [aws_lb_target_group.this.arn]
  health_check_type        = "ELB"
  custom_tags              = var.custom_tags
}

module "alb" {
  source = "../../networking/alb"

  alb_name   = "hello-world-${var.environment}"
  subnet_ids = data.aws_subnets.default.ids
}
