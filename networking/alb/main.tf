locals {
  http_port    = 80
  any_port     = 0
  any_protocol = "-1"
  tcp_protocol = "tcp"
  all_ips      = ["0.0.0.0/0"]
}

resource "aws_lb" "this" {
  name               = "${var.alb_name}-alb"
  load_balancer_type = "application"
  subnets            = var.subnet_ids
  security_groups    = [aws_security_group.this.id]
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = aws_lb.this.arn
  port              = local.http_port
  protocol          = "HTTP"

  # By default, return a simple 404 page.
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: Page not found."
      status_code  = 404
    }
  }
}

resource "aws_security_group" "this" {
  name = "${var.alb_name}-alb-sg"
}

# Allow inbound HTTP requests.
resource "aws_security_group_rule" "this_inbound" {
  type              = "ingress"
  security_group_id = aws_security_group.this.id
  from_port         = local.http_port
  to_port           = local.http_port
  protocol          = local.tcp_protocol
  cidr_blocks       = local.all_ips
}

# Allow all outbound requests.
resource "aws_security_group_rule" "this_outbound" {
  type              = "egress"
  security_group_id = aws_security_group.this.id
  from_port         = local.any_port
  to_port           = local.any_port
  protocol          = local.any_protocol
  cidr_blocks       = local.all_ips
}
