variable "alb_name" {
  description = "The name to use for this ALB."
  type        = string
}

variable "subnet_ids" {
  description = "Subnets."
  type        = list(string)
}
