variable "aws_region" {
  description = "The AWS region to deploy to."
  type        = string
  default     = "us-east-1"
}

variable "server_port" {
  description = "The port the server will use for HTTP requests."
  type        = number
  default     = 8080
}

variable "cluster_name" {
  description = "The name to use for all the cluster resources."
  type        = string
}

variable "ami" {
  description = "The ID of the AMI that should be used to create the instances."
  type        = string
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)."
  type        = string
  default     = "t2.micro"
}

variable "desired_capacity" {
  description = "The desired number of EC2 Instances running at a given time."
  type        = number
}

variable "min_size" {
  description = "The minimum number of EC2 Instances in the ASG."
  type        = number
}

variable "max_size" {
  description = "The maximum number of EC2 Instances in the ASG."
  type        = number
}

variable "scaling_schedule_enabled" {
  description = "Enable scheduled actions in the ASG."
  type        = bool
  default     = false
}

variable "custom_tags" {
  description = "Custom tags to set on the Instances in the ASG."
  type        = map(string)
  default     = {}
}

variable "subnet_ids" {
  description = "The subnet IDs to deploy to."
  type        = list(string)
}

variable "target_group_arns" {
  description = "The ARNs of ELB target groups in which to register Instances."
  type        = list(string)
  default     = []
}

variable "health_check_type" {
  description = "The type of health check to perform."
  type        = string
  default     = "EC2"

  validation {
    condition     = can(regex("^(EC2|ELB)$", var.health_check_type))
    error_message = "'health_check_type' Must be one of: EC2 or ELB."
  }
}

variable "user_data" {
  description = "The User Data script to run at boot time."
  type        = string
  default     = null
}
