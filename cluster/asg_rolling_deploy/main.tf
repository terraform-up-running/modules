locals {
  tcp_protocol = "tcp"
  all_ips      = ["0.0.0.0/0"]
}

resource "aws_launch_template" "this" {
  name                   = "${var.cluster_name}-lt"
  image_id               = var.ami
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.this.id]
  user_data              = var.user_data
}

resource "aws_security_group" "this" {
  name = "${var.cluster_name}-sg"
}

resource "aws_security_group_rule" "this" {
  type              = "ingress"
  security_group_id = aws_security_group.this.id
  from_port         = var.server_port
  to_port           = var.server_port
  protocol          = local.tcp_protocol
  cidr_blocks       = local.all_ips
}


# Austoscaling Group (ASG) setup starts here.
resource "aws_autoscaling_group" "this" {
  name                = "${var.cluster_name}-asg"
  vpc_zone_identifier = var.subnet_ids
  target_group_arns   = var.target_group_arns
  health_check_type   = var.health_check_type
  desired_capacity    = var.desired_capacity
  min_size            = var.min_size
  max_size            = var.max_size

  launch_template {
    id      = aws_launch_template.this.id
    version = aws_launch_template.this.latest_version
  }

  instance_refresh {
    strategy = "Rolling"
    triggers = ["tag"]

    preferences {
      min_healthy_percentage = 50
    }
  }

  tag {
    key                 = "Name"
    value               = "${var.cluster_name}-ec2"
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = var.custom_tags

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

resource "aws_autoscaling_schedule" "this_in" {
  count = var.scaling_schedule_enabled ? 1 : 0

  autoscaling_group_name = aws_autoscaling_group.this.name
  scheduled_action_name  = "scale-out-during-business-hours"
  desired_capacity       = 10
  min_size               = 2
  max_size               = 10
  recurrence             = "0 9 * * *"
}

resource "aws_autoscaling_schedule" "this_out" {
  count = var.scaling_schedule_enabled ? 1 : 0

  autoscaling_group_name = aws_autoscaling_group.this.name
  scheduled_action_name  = "scale-in-at-night"
  desired_capacity       = 2
  min_size               = 2
  max_size               = 10
  recurrence             = "0 17 * * *"
}
