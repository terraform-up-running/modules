resource "aws_db_instance" "this" {
  identifier_prefix       = var.db_identifier_prefix
  allocated_storage       = var.db_allocated_storage
  instance_class          = var.db_instance_class
  skip_final_snapshot     = true
  backup_retention_period = var.backup_retention_period
  replicate_source_db     = var.replicate_source_db

  # Only set these parameters if replicate_source_db is not set.
  engine   = var.replicate_source_db == null ? "mysql" : null
  db_name  = var.replicate_source_db == null ? var.db_name : null
  username = var.replicate_source_db == null ? var.db_username : null
  password = var.replicate_source_db == null ? var.db_password : null
}
