variable "db_identifier_prefix" {
  description = "Identifier to prepend to the name of the database instance."
  type        = string
  default     = ""
}

variable "db_allocated_storage" {
  description = "DB allocated storage for RDS instance."
  type        = number
  default     = null
}

variable "db_instance_class" {
  description = "The type of DB instance to be deployed."
  type        = string
  default     = "db.t3.micro"
}

variable "db_name" {
  description = "The name of the DB for your application."
  type        = string
  default     = null

  validation {
    condition     = var.db_name == null || can(regex("^[a-zA-Z][[:alnum:]]*$", var.db_name))
    error_message = "'db_name' Must begin with a letter and contain only alphanumeric characters."
  }
}

variable "db_username" {
  description = "DB administrative username."
  type        = string
  sensitive   = true
  default     = null
}

variable "db_password" {
  description = "DB administrative password."
  type        = string
  sensitive   = true
  default     = null
}

variable "backup_retention_period" {
  description = "Backup retention days."
  type        = number
  default     = 0

  validation {
    condition     = var.backup_retention_period >= 0 && var.backup_retention_period <= 35
    error_message = "Must be between 1 and 35 to enable replication."
  }
}

variable "replicate_source_db" {
  description = "If specified, replicate the RDS database at the ARN provided."
  type        = string
  default     = null
}
